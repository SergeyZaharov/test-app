/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import store from './store';
import { Provider } from 'react-redux';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './components/Home';
import ImageView from './components/ImageView';

const Stack = createStackNavigator();

const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={Home}
            options={{ title: 'Список' }}
          />
          <Stack.Screen
          name="ImageView"
          component={ImageView}
          options={{ title: 'Изображение' }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};



export default App;
