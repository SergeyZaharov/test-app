import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {
  ActivityIndicator,
  Alert,
  SafeAreaView,
  View,
} from 'react-native';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getImagesAction, setImageAction} from '../../store/actions';

import ImagesList from '../ImagesList';

import * as selectors from '../../store/selectors';

const mapStateToProps = state => ({
    list: selectors.getList(state),
    error: selectors.getError(state),
});

const mapDispatchToProps = dispatch => ({
    getImages: bindActionCreators(getImagesAction, dispatch),
    setImage: bindActionCreators(setImageAction, dispatch),
});


const Home = ({navigation, list, setImage, getImages, error}) => {

    useEffect(() => {
        getImages();
    }, []);

    if (error.isError) {
        Alert.alert('Ошибка', error.message);
    }

    return (
        <SafeAreaView style={styles.main}>
            {!list.length
            ? <View><ActivityIndicator size="large" color="#0000ff" /></View>
            : <ImagesList list={[...list]} navigation={navigation} setImage={setImage} />
      }
        </SafeAreaView>
    )
};

Home.propTypes = {
    navigation: PropTypes.object,
    list: PropTypes.arrayOf(PropTypes.shape({
        urls: PropTypes.shape({
            full: PropTypes.string.isRequired,
            small: PropTypes.string.isRequired,
        }),
        user: PropTypes.shape({
            name: PropTypes.string,
        })

    })),
    setImage: PropTypes.func,
    getImages: PropTypes.func,
    error: PropTypes.shape({
        isError: PropTypes.bool,
        message: PropTypes.string,
    })
}

const styles = {
    main: {
        flex:1,
        justifyContent: 'center',
        height: '100%'
    },
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);