import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
    View,
} from 'react-native';

import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';

import * as selectors from '../../store/selectors';

const mapStateToProps = state => ({
  selectedImage: selectors.getSelectedImage(state),
})

const ImageView = ({selectedImage}) => {

    return (
        <View style={styles.imgContainer}>
    <Image 
        indicator={ProgressBar} 
        source={selectedImage}
        style={styles.image}
        resizeMode={'contain'}
    />
        </View>
    )
}

ImageView.propTypes = {
    selectedImage: PropTypes.shape({
        uri: PropTypes.string,
    }),
}

const styles = {
    image: {
        height: '100%',
        width: '100%',
    },
    imgContainer: {
        backgroundColor: '#c3c3c3',
        height: '100%'
    },
}

export default connect(mapStateToProps)(ImageView);