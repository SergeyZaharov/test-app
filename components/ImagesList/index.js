import React from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const defaultTitle = 'Без названия';

const getImgTitle = item => {
	return item.description || item.alt_description ||  defaultTitle;
}

const imgRow = (navigation, items, key, setImage) => {
	let row = items.map(item => { 
		return (
	        <View style={styles.card} key={item.urls.small}>
	            <View style={styles.imgContainer}>
		            <TouchableOpacity onPressIn={() => setImage(item.urls.full)} onPress={() => navigation.navigate('ImageView') }>
			            <Image style={styles.imageTile} source={{uri: item.urls.small}} resizeMode={'cover'}/>
		            </TouchableOpacity>
		        </View>
	            <View style={styles.infoBlock}>
	            	<View style={styles.titleBlock}>
		            	<Text style={styles.title}>{getImgTitle(item)}</Text>
	            	</View>
	            	<View style={styles.authorBlock}>
		            	<Text style={styles.authorContainer}><Text style={styles.author}>Автор:</Text> {item.user.name}</Text>
	            	</View>
	            </View>
	        </View>
		);
	}
)

	return row;

}

const ImagesList = ({navigation, list, setImage}) => {
	let rows = [];

	while (list.length) {
		rows.push(imgRow(navigation, list.splice(0, 3), list.length+1, setImage));
	}

	return (
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          contentContainerStyle={{paddingBottom:100}}
          >
        <View style={{flex:1, flexDirection: 'column'}}>
          {rows}
        </View>
        </ScrollView>
	)
}

ImagesList.propTypes = {
    navigation: PropTypes.object,
    list: PropTypes.arrayOf(PropTypes.shape({
        urls: PropTypes.shape({
            full: PropTypes.string.isRequired,
            small: PropTypes.string.isRequired,
        }),
        user: PropTypes.shape({
            name: PropTypes.string,
        })

    })),
    setImage: PropTypes.func,
}

const styles = StyleSheet.create({
	author: {
		fontWeight: 'bold',
		fontSize: 16
	},
	authorBlock: {
		flex: 1,
	},
	authorContainer: {
		flex: 1,
		position: 'absolute',
		bottom: 0,
		fontSize: 16,
		fontFamily: 'sans-serif'
	},
	card: {
		flex:1,
		flexDirection: 'row',
		backgroundColor: 'black',
		marginHorizontal: 15,
		marginVertical: 7,
		minHeight: 100,
		elevation: 12
	},
	imagesContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
		flexWrap: 'wrap',
		alignItems: 'center',
	},
	imageTile: {
		backgroundColor: '#c3c3c3',
		flex: 1,
		width: '100%',
		minHeight: 100,
	},
	imgContainer: {
		flex:2,
		minHeight: 100,
		backgroundColor: '#f5f5f5',
		padding: 0,
		justifyContent: 'center'
	},
	infoBlock: {
		flex:4,
		backgroundColor: 'white',
		padding:10,
		flexDirection: 'column'
	},
	title: {
		flex:1,
		fontWeight: '700' ,
		fontSize: 18,
		fontFamily: 'sans-serif'
	},
	titleBlock: {
		flex: 2,
		paddingBottom: 25
	},
});

export default ImagesList;
