import * as actions from './actionTypes';

import {
	UNSPLASH_CLIENT_ID,
	UNSPLASH_API_URI
} from '../constants';

export const requestImagesAction = () => ({
	type: actions.IMAGES_GET_LIST_REQUEST,
});

export const responseImagesAction = list => ({
	type: actions.IMAGES_GET_LIST_RESPONSE,
	payload: {list},
});

export const setImageAction = uri => ({
	type: actions.IMAGES_SET_ONE,
	payload: {uri},
});

export const errorImagesAction = message => ({
	type: actions.IMAGES_GET_LIST_ERROR,
	payload: {message},
})

export const getImagesAction = () => dispatch => {

	dispatch(requestImagesAction());
	return fetch(`${UNSPLASH_API_URI}/photos/?client_id=${UNSPLASH_CLIENT_ID}`, {
		method: 'GET',
	})
	.then(response => {
		if (response.status !== 200) {
			var error = new Error(response.statusText);
			error.response = response;
			throw error;
		}

		return response.json();
	})
	.then(data => {
		dispatch(responseImagesAction(data));
	})
	.catch(error => dispatch(errorImagesAction(error.message)));
}
