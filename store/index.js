import { createStore} from 'redux';
import reducer from './reducer';
import { applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

const middlewares = [thunk];

const store = createStore(combineReducers({
  main: reducer,
}), applyMiddleware(...middlewares))

export default store;