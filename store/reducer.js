import * as types from './actionTypes.js';

const initialState = {
	images: {
		list: [],
		inRequest: false,
		selectedImage: null,
	},
	error: {
		message: '',
		isError: false,
	}
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case types.IMAGES_GET_LIST_REQUEST:
		return {
			...state,
			error: {
				isError: false,
				message: '',
			},
			images: {...state.images, inRequest: true},
		};

		case types.IMAGES_GET_LIST_RESPONSE:
		let images = [...action.payload.list].map(item => ({urls: item.urls}));

		return {
			...state,
			images: {...state.images, list: [...action.payload.list], inRequest: false},
		};

		case types.IMAGES_GET_LIST_ERROR:
		return {
			...state,
			images: initialState.images,
			error: {
				isError: true,
				message: action.payload.message,
			}
		}


		case types.IMAGES_SET_ONE:
		return {
			...state,
			images: {...state.images, selectedImage: {uri: action.payload.uri}}
		}

		default:
		return state;
	}
}

export default reducer;
