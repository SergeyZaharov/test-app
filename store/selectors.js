export const getList = state => state.main.images.list;

export const getError = state => state.main.error;

export const getSelectedImage = state => state.main.images.selectedImage || {uri: ''};
